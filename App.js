import React, {useEffect} from 'react';
import { Text, View,  } from 'react-native';

import HomeScreen from './src/screens/home';

import SplashScreen from 'react-native-splash-screen';

const App = () => {

    useEffect(() => {
        setTimeout(() => {
            SplashScreen.hide()
        }, 1000);
    }, [])

    return (
        <>
            <HomeScreen />
        </>
    )
}

export default App