const baseUrl = 'https://stagingsite.livelaw.in/dev/h-api/'

export function getResponse(query, token) {
    return(
        fetch(
            baseUrl + 'news',
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    's-id': 'CKEY0F1HNJGSZHJFQPYB5HBMJEM79K26YQDJTY0RX7MVPHGHXTKALSTVARSDAYKUGF2Y',
                },
            }
        )
        .then(res => res.json())
    );
}
