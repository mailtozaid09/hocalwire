import React, {useState, useEffect} from 'react';
import { Text, View, StyleSheet, FlatList, Image, TouchableOpacity, Dimensions, } from 'react-native';
import { media } from '../../global/media';
import { colors } from '../../global/colors';
import { formatHours } from '../../global/constants';

const NewsItem = ({data, index}) => {

    const placeholderText = 'Lorem ipsum dolor sit amet'
    const placeholderImg = 'https://images.unsplash.com/photo-1555066931-4365d14bab8c?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8NXx8Y29kaW5nfGVufDB8fDB8fHww&auto=format&fit=crop&w=800&q=60'
    
    return (
        <View key={data?.mediaId} style={styles.container} >
            <Image source={{uri: data?.mediaId || placeholderImg}} style={styles.authorImage}  />

            <View style={styles.india} >
                <Text style={styles.title} >India</Text>
            </View>
            <View style={{padding: 12}} >
                <Text style={styles.description} >{data?.description || placeholderText}</Text>
                
                <View style={styles.bottomContainer} >
                    <View style={styles.authorContainer} >
                        <Image source={media.user} style={styles.avatar} />
                        <Text style={styles.title} >{data?.authorName}</Text>
                    </View>

                    <View style={styles.authorContainer} >
                        <Image source={media.clock} style={styles.avatar} />
                        <Text style={styles.title} >{formatHours(data?.date_news)} hrs ago</Text>
                    </View>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        marginBottom: 14,
        borderRadius: 10,
        borderWidth: 1,
        backgroundColor: colors.light_gray,
    },
    bottomContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    authorContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    authorImage: {
        height: 220,
        width: '100%',   
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
    },
    title: {
        fontSize: 11,
        color: colors.black,
        fontWeight: 'bold',
    },
    description: {
        fontSize: 16,
        fontWeight: 'bold',
        color: colors.black,
        marginBottom: 10,
    },
    avatar: {
        height: 22,
        width: 22,
        marginRight: 6
    },
    india: {
        position: 'absolute',
        height: 30,
        top: 20,
        paddingHorizontal: 15,
        borderRadius: 4,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.yellow,
    }
})

export default NewsItem