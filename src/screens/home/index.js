import React, {useState, useEffect} from 'react';
import { Text, View, StyleSheet, FlatList, Image, TouchableOpacity, Dimensions, } from 'react-native';
import { getResponse } from '../../api/api';
import { colors } from '../../global/colors';
import NewsItem from '../../components/news/NewsItem';


const HomeScreen = ({navigation}) => {

    const [newsArray, setNewsArray] = useState([]);

    useEffect(() => {
        getResponse()
        .then((resp) => {
            //console.log("resp => ", resp);
            setNewsArray(resp?.news)
        })
        .catch((err) => {
            console.log("err => ", err);
        })
    }, [])

    const EmptyData = () => {
        return(
            <View style={styles.emptyContainer}> 
                <Text>No Data Found!</Text>
            </View>
        )
    }

    return (
        <View style={styles.container} >
            <View style={{width: '100%', }} >
                <FlatList
                    data={newsArray}
                    keyExtractor={item => item.newsId}
                    ListEmptyComponent={EmptyData}
                    renderItem={({item, index}) => (
                        <NewsItem data={item} index={index} />
                    )}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        alignItems: 'center',
        backgroundColor: colors.primary,
    },
    emptyContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
})

export default HomeScreen