import moment from "moment"
import { Dimensions } from "react-native"


export const screenWidth = Dimensions.get('window').width
export const screenHeight = Dimensions.get('window').height

export const formatDate = (date) => {
    return moment(date).local().format('ddd, DD-MM-YYYY | HH:mm A')
}   

export const formatHours = (date) => {

    const currentDate = new Date()
    var d1 = moment(currentDate).local().format('HH:mm:ss a')
    var d2 = moment(date).local().format('HH:mm:ss a')

    var startTime = moment(d1, 'hh:mm:ss a');
    var endTime = moment(d2, 'hh:mm:ss a');

    var d = endTime.diff(startTime, 'hours');
            
    return d
}   
