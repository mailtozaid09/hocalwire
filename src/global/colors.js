export const colors = {
    primary: '#ffffff',
    light_red: '#FEA7A9',

    yellow: '#ffab40',
    
    black: '#000000',
    white: '#fdfdfd',
    
    gray: '#919399',
    light_gray: '#ebecf0',
    
    blue: '#81b3f3',
    red: '#f29999',
    orange: '#f7c191',
    reddish: '#ED4545',
}